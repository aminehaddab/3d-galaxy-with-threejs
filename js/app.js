import * as THREE from "three";
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import {GLTFLoader} from 'three/examples/jsm/loaders/GLTFLoader.js';
import {DRACOLoader} from 'three/examples/jsm/loaders/DRACOLoader.js';
import {CSS2DRenderer, CSS2DObject} from 'three/examples/jsm/renderers/CSS2DRenderer';
import fragment from "./shader/fragment.glsl";
import vertex from "./shader/vertex.glsl";
import vertex2 from "./shader/vertex2.glsl";
import GUI from 'lil-gui'; 
import gsap from "gsap";
import particleTexture from '../particle.png';
import planet1Pic from '../planet2.png';

function lerp (a,b,t){ 
  return (a-.5)* (1-t) + b+t;
}

export default class Sketch {
  constructor(options) {
    this.scene = new THREE.Scene();

    this.container = options.dom;
    this.width = this.container.offsetWidth;
    this.height = this.container.offsetHeight;
    this.renderer = new THREE.WebGLRenderer();
    this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
    this.renderer.setSize(this.width, this.height);
    this.renderer.setClearColor(0x000000, 1); 
    this.renderer.physicallyCorrectLights = true;
    this.renderer.outputEncoding = THREE.sRGBEncoding;

    this.raycaster = new THREE.Raycaster();
    this.pointer = new THREE.Vector2();
    this.point = new THREE.Vector3()
    
    this.container.appendChild(this.renderer.domElement);

    this.camera = new THREE.PerspectiveCamera(
      70,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );

    this.camera.position.set(0, 1, -3);

    /////////////
    /////////////
    /////////////
    /////////////
    this.labelRenderer = new CSS2DRenderer();
    this.labelRenderer.setSize(window.innerWidth, window.innerHeight);
    this.labelRenderer.domElement.style.position = 'absolute';
    this.labelRenderer.domElement.style.top = '0px';
    this.labelRenderer.domElement.style.pointerEvents = 'none';
    document.body.appendChild(this.labelRenderer.domElement);
   
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.time = 0;
    this.materials = []
    this.isPlaying = true;

    let opts = [
      {
      min_radius: 0.1,
      max_radius: 1,
      color: '#621b7b',
      size: 1,
      },
      {
        min_radius: 0.1,
        max_radius: 1,
        color: '#018dff',
        size: 1,
      }
    ]
    
    opts.forEach(op=>{
      this.addObject(op)
      this.addObjectScattered(op)
    })
    this.addSphere();  
    this.raycasterEvent();
    this.resize();
    this.render();
    this.setupResize();
    this.addLights();
    
    // this.settings();

  }

  addSphere(){

    const planetTexture = new THREE.TextureLoader().load(planet1Pic);
    const earthGeometry = new THREE.SphereGeometry(0.15,32,32);
    const earthMateriel = new THREE.MeshStandardMaterial({
      map:planetTexture,
      transparent: true,
    });
    const earthMesh = new THREE.Mesh(earthGeometry,earthMateriel);
    earthMesh.position.set(0.70,0.5,-1.2);
    this.scene.add(earthMesh);

    const divP = document.createElement('div');
    const planetPar1 = document.createElement('p');
    planetPar1.textContent = "This is a planet !";
    divP.appendChild(planetPar1)
    const planetPar2 = document.createElement('p');
    planetPar2.textContent = "We can put some CSS to make things look even better.";
    divP.appendChild(planetPar2)
    divP.style = 'color: white; text-align: center;';
    const planetLabel = new CSS2DObject(divP); 
    planetLabel.position.set(0,0.25,0);
    
    earthMesh.add(planetLabel);
    
  }

  raycasterEvent(){
    let mesh = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(10,10,10,10).rotateX(-Math.PI/2),
      new THREE.MeshBasicMaterial({color: 0xff0000, wireframe: true})
    )

    let test = new THREE.Mesh(
      new THREE.SphereBufferGeometry(0.1,10,10),
      new THREE.MeshBasicMaterial({color: 0xff0000, wireframe: true})
    )
   

    window.addEventListener('pointermove', (event)=>{
      this.pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
      this.pointer.y = - (event.clientY / window.innerHeight) * 2 + 1;
      this.raycaster.setFromCamera( this.pointer, this.camera);
      const intersects = this.raycaster.intersectObjects( [mesh] );
      if(intersects[0]){
        test.position.copy(intersects[0].point)
        this.point.copy(intersects[0].point)
      }
    });

  }

  settings() {
    let that = this;
    this.settings = {
      progress: 0,
    };
    this.gui = new GUI();
    this.gui.add(this.settings, "progress", 0, 1, 0.01);
  }

  setupResize() {
    window.addEventListener("resize", this.resize.bind(this));
  }

  resize() {
    this.width = this.container.offsetWidth;
    this.height = this.container.offsetHeight;
    this.renderer.setSize(this.width, this.height);
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.labelRenderer.setSize(this.width, this.height)
  }

  addObject(opts) {
    let that = this;
    let count = 10000;
    let min_radius = opts.min_radius; 
    let max_radius = opts.max_radius;
    let particlegeo = new THREE.PlaneGeometry(1,1);
    let geo = new THREE.InstancedBufferGeometry();
    geo.instanceCount = 10000;
    geo.setAttribute('position', particlegeo.getAttribute('position'));
    geo.index = particlegeo.index;

    let pos = new Float32Array(count*3);

    for(let i = 0; i < count; i++){
      let theta = Math.random()*2*Math.PI;
      let r = lerp(min_radius, max_radius, Math.random()*1.15);
      let x = r*Math.sin(theta);
      let y = (Math.random()-0.5)*0.1;
      let z = r*Math.cos(theta);

      pos.set([
        x,y,z
      ],i*3);
    }

    geo.setAttribute('pos', new THREE.InstancedBufferAttribute(pos,3,false));

    let material = new THREE.ShaderMaterial({
      extensions: {
        derivatives: "#extension GL_OES_standard_derivatives : enable"
      },
      side: THREE.DoubleSide,
      uniforms: {
        uTexture: { value: new THREE.TextureLoader().load(particleTexture) },
        time: { value: 0 },
        uMouse : { value: new THREE.Vector3() },
        size: { value: opts.size },
        uColor: { value: new THREE.Color(opts.color) },
        resolution: { value: new THREE.Vector4() },
      },
      // wireframe: true,
      transparent: true,
      depthTest: false,
      vertexShader: vertex2,
      fragmentShader: fragment
    });
    this.materials.push(material);
    this.geometry = new THREE.PlaneGeometry(1, 1, 1, 1);

    this.points = new THREE.Mesh(geo, material);
    this.scene.add(this.points);

  }

  addObjectScattered(opts) {
    let that = this;
    let count = 10000;
    let min_radius = 0.5; 
    let max_radius = 3;
    let particlegeo = new THREE.PlaneGeometry(1,1);
    let geo = new THREE.InstancedBufferGeometry();
    geo.instanceCount = 10000;
    geo.setAttribute('position', particlegeo.getAttribute('position'));
    geo.index = particlegeo.index;

    let pos = new Float32Array(count*3);

    for(let i = 0; i < count; i++){
      let theta = Math.random()*2*Math.PI;
      let r = lerp(min_radius, max_radius, Math.random()*1.15);
      let x = r*Math.sin(theta);
      let y = (Math.random()-0.5)*0.1;
      let z = r*Math.cos(theta);

      pos.set([
        x,y,z
      ],i);
    }

    geo.setAttribute('pos', new THREE.InstancedBufferAttribute(pos,3,false));

    let material = new THREE.ShaderMaterial({
      extensions: {
        derivatives: "#extension GL_OES_standard_derivatives : enable"
      },
      side: THREE.DoubleSide,
      uniforms: {
        uTexture: { value: new THREE.TextureLoader().load(particleTexture) },
        time: { value: 0 },
        uMouse : { value: new THREE.Vector3() },
        size: { value: opts.size },
        uColor: { value: new THREE.Color(opts.color) },
        resolution: { value: new THREE.Vector4() },
      },
      // wireframe: true,
      transparent: true,
      depthTest: false,
      vertexShader: vertex2,
      fragmentShader: fragment
    });
    this.materials.push(material);
    this.geometry = new THREE.PlaneGeometry(1, 1, 1, 1);

    this.points = new THREE.Mesh(geo, material);
    this.scene.add(this.points);

  }

  stop() {
    this.isPlaying = false;
  }

  play() {
    if(!this.isPlaying){
      this.isPlaying = true;
      this.render()
    }
  }

  addLights(){
    const light1  = new THREE.AmbientLight(0xffffff, 3);
    this.scene.add( light1 );

    /* const light2  = new THREE.DirectionalLight(0xffffff, 0.8 * Math.PI);
    light2.position.set(0.5, 0.6, 0.866); // ~60º
    this.scene.add( light2 ); */
  }

  render() {
    
    if (!this.isPlaying) return;
    this.time += 0.05;
    this.materials.forEach(m=>{
      m.uniforms.time.value = this.time*0.1 ;
      m.uniforms.uMouse.value = this.point ;
    })
    
    requestAnimationFrame(this.render.bind(this));
    this.renderer.render(this.scene, this.camera);
    this.labelRenderer.render(this.scene, this.camera);
  }
}

new Sketch({
  dom: document.getElementById("container")
});
