# 3D-GALAXY-WITH-THREEJS

## Author
Amine Haddab

## Description
This project utilizes Three.js to create a 3D representation of a galaxy along with a planet. It involves mathematical concepts to render the celestial bodies in a visually appealing manner.

## Installation
To get started with the project, follow these steps:

1. Clone the repository:
```bash
git clone https://gitlab.com/aminehaddab/3d-galaxy-with-threejs.git
```

2. Install Parcel bundler globally (if not already installed):
```bash
npm install parcel --global
```

3. Install project dependencies:
```bash
npm install @parcel/config-default three lil-gui gsap
npm install @parcel/transformer-glsl
```

4. Run the project:
```bash
parcel index.html
```

This will start a local server and open the project in your default web browser.